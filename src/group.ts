export class GroupItem {
// элемент группы

  left: number        // номер левого пикселя 
  right: number       // номер правого пикселя 
  bind: number[]      // номера групп предыдущего уровня, с которым связан данный элемент
}
  
export class GroupRow {
// несколько элеменов групп для строки матрицы
  private row: GroupItem[] = new Array()

  getItem(k: number) :GroupItem  { return this.row[k] }
  getLength() :number { return this.row.length }
  push(item: GroupItem) { return this.row.push(item)}
}
  
export class Group {
// группы точек для всей матрицы по некоторому критерию - например непрерывная последовательность единиц 
// для каждой строки свой набор 

    // номер элемента в массиве соотв-т номеру строки в матрице
    private arBind: GroupRow[] = null

    // прямоугольник
    width : number = null
    height :number = null

    getRow(k: number): GroupRow  { return this.arBind[k] }
    getLength() :number  { return this.arBind.length }
    
    constructor(height: number, width: number) {
      this.height = height
      this.width = width 
      this.arBind = new Array(height)
      for (let i=0; i<this.getLength(); i++)   this.arBind[i] = new GroupRow()
    }
  
    print(type: string) {
      console.log('Разбивка по группам')
      if (type == 'matrix')
       {
        for (let y=0; y<this.height; y++) {
          let raw = ''
          for (let x=0; x<this.width; x++) {
            let inGroup = -1
            // берем все группы для текущей строки
            for (let j=0; j<this.getRow(y).getLength(); j++) {
              let g = this.getRow(y).getItem(j)
              if (g.left <= x && x <= g.right) { inGroup = j; break; }
              // дальше искать смысла нет
              if (g.left > x) break
            }
            if (inGroup >= 0) 
              raw += inGroup
            else raw += '-'
            raw += ' ' 
          }
          console.log(raw)
        }
      }
      else 
      {
        for (let i=0; i<this.getLength(); i++) {
          let row = ''
          for (let j=0; j<this.getRow(i).getLength(); j++) {
            let g = this.getRow(i).getItem(j)
            row += '[' + g.left + '-' + g.right + ']' + '{' + g.bind + '}'
          }
          console.log(row)
        }
      }
      console.log()
    }
   
  } 
  