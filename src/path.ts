import { Group } from "./group"
import { Image } from "./image"

// пути  
export class Path {
 
  private matrix :number[][] = []

  constructor(matrix? :number[][]) {
    if (matrix) {
      this.matrix = []
      for (let i=0; i<matrix.length; i++) {
        this.matrix[i] = [...matrix[i]]
      }
    } 
  }

  getMatrix() :number[][] { return this.matrix }

  // формируем новую - ко всем путям одной матрицы добавляем пути другой через матрицу связей
  public add(next :Path, bind :Path ) :void {
    if (!bind) throw 'Не заданы связи между путями'
    if (bind.getMatrix().length == 0) return 
    if (bind.getMatrix()[0].length != 2) throw 'Длина путей в матрице связей должна быть 2'

    let m1 = this.matrix
    let b = bind.matrix
    let m2 = next.matrix

    let mNew = []
    // идем по всем связям между 1-й и 2-й матрицами
    for (let i=0; i<b.length; i++) {
      // посл-й элем в 1-й матрице
      let last = b[i][0]
      // первый во 2-й
      let first = b[i][1]
      // фильтруем все пути в матрице1 и матрице 2 
      let m1New :number[][] = this.filter(m1, last, 'last')
      let m2New :number[][] = this.filter(m2, first, 'first')
      // связи между кусками искались на полных снимках - в дороги могли и не попасть
      if (m1New.length == 0 || m2New.length == 0) continue

      let merge12 = this.merge(m1New, m2New) 
      mNew.push(...merge12)
    } 
    // если куски никак не связаны, то просто соединяем m1 m2
    if (mNew.length == 0) {

    }
    this.matrix = mNew
  }

  // уберем группы менее заданного 
  public cutLess(limit :number, measure :string) {
    if (measure != 'group' && measure != 'pixel') throw 'Ошибка в параметрах cutLess'
    if (measure == 'pixel') throw 'Измерение длины дороги в пикселях пока не поддерживается'
    let newM = []
    let m = this.getMatrix()
    for (let i=0; i<m.length; i++) {
      // сама дорога - последовательность номеров групп - прерывается null
      let p = m[i]
      let sequence = []
      for (let j=0; j<p.length; j++) {
        if (p[j] == null) {
          // если непрерывная последовательность меньше предела, то ее удаляем 
          if (sequence.length < limit) {
            for (let k=0; k<sequence.length; k++) p[sequence[k]] = null
            sequence = []
          }
        }
        else
          sequence.push(j)
      }
      // при выходе из цикла по каждому пути тоже проверим
      if (sequence.length < limit) {
        for (let k=0; k<sequence.length; k++) p[sequence[k]] = null
        sequence = []
      }
    }
    // уберем полностью пустые
    for (let i=0; i<m.length; i++) {
      let p = m[i]
      let allIsNull = true
      for (let j=0; j<p.length; j++) {
        if (p[j] > 0) { allIsNull = false; break; }
      }
      if (!allIsNull) newM.push(p)
    }
    // и присвоим исходной матрице
    this.matrix = newM
  }

  // вырезаем кусок из матрицы
  public extract(y, dy) :Path {
    let newM = []
    let m = this.matrix
    for (let i=0; i<m.length; i++)  {
      let piece = m[i].slice(y, y+dy)
      newM.push(piece)
    }
    return new Path(newM)
  }


  private merge(m1 :number[][], m2 :number[][]) :number[][] {
  // просто к каждому пути 1-й матрицы добавляем все пути 2-й матрицы
    let ret = []
    for (let k=0; k<m1.length*m2.length; k++) ret[k] = []
    for (let i=0; i<m1.length; i++) {
      for (let j=0; j<m2.length; j++) {
        ret[i * m2.length + j].push(...m1[i])
        ret[i * m2.length + j].push(...m2[j])
      }
    }
    return ret
  }

  private filter(m :number[][], value :number, place :string ) :number[][] {
  // возвращаем только те пути которые удовлетворяют условию (без копирования путей)
    if (place != 'first' && place != 'last') throw 'Ждали first or last'
    let ret = []
    for (let i=0; i<m.length; i++) {
      let p = m[i]
      let index = 0
      if (place == 'last') index = p.length - 1
      if (p[index] == value) ret.push(p)
    }
    return ret
  }

  public print(rem: string) { 
    console.log(rem)
    if (this.matrix.length == 0) return
    let height = this.matrix[0].length
    for (let j=0; j<height; j++) {
      let row = ''
      for (let i=0; i<this.matrix.length; i++) {
        let v = String(this.matrix[i][j])
        if (v == 'null') v = '-' 
        row +=  v + ' ' 
      }
      console.log(row)
    }
  }

}