// I wrote a sobel edge detector in Javascript!
// todo: try Laplacian of Gaussian (LoG) instead of Sobel

// Example usage (taking the pxiels 1D):
// const edge = createEdgeMapFromImageData(imageData);
// for (const i in edge) {
//   let x = i % canvas.width;
//   let y = (i - x) / canvas.width;
//   ctx.fillStyle = `rgba(${edge[i]},${edge[i]},${edge[i]},255)`;
//   ctx.fillRect(x, y, 1, 1 );
// }

var kernelX = [
  [-1, 0, 1],
  [-2, 0, 2],
  [-1, 0, 1]
];

var kernelY = [
  [-1, -2, -1],
  [ 0,  0,  0],
  [ 1,  2,  1]
];


// input: imageData object with RGBA data
// output: 2D array with edge detection data. Note, 1 channel per pixel.
export default function createEdgeMapFromImageData(imageData ) :Uint8ClampedArray {
  let width = imageData.width
  let height = imageData.height
  let data = imageData.data

  // create greyscale first
  let grey = new Uint8ClampedArray(imageData.data.length * 0.25)
  {
    for (let i=0; i<grey.length; i++) {
      let r = data[i*4];
      let g = data[i*4+1];
      let b = data[i*4+2];
      let a = data[i*4+3];
      grey[i] = (r + g + b)/3; // average
    }
  }

  let sobelData = new Uint8ClampedArray(grey.length)
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      //console.log('\t\t\t\t x = ' + x + ' y = ' + y)
      var pixelX = (
          (kernelX[0][0] * pixelAt(grey, width, height, x - 1, y - 1)) +
          (kernelX[0][1] * pixelAt(grey, width, height, x, y - 1)) +
          (kernelX[0][2] * pixelAt(grey, width, height, x + 1, y - 1)) +
          (kernelX[1][0] * pixelAt(grey, width, height, x - 1, y)) +
          (kernelX[1][1] * pixelAt(grey, width, height, x, y)) +
          (kernelX[1][2] * pixelAt(grey, width, height, x + 1, y)) +
          (kernelX[2][0] * pixelAt(grey, width, height, x - 1, y + 1)) +
          (kernelX[2][1] * pixelAt(grey, width, height, x, y + 1)) +
          (kernelX[2][2] * pixelAt(grey, width, height, x + 1, y + 1))
      );

      var pixelY = (
        (kernelY[0][0] * pixelAt(grey, width, height, x - 1, y - 1)) +
        (kernelY[0][1] * pixelAt(grey, width, height, x, y - 1)) +
        (kernelY[0][2] * pixelAt(grey, width, height, x + 1, y - 1)) +
        (kernelY[1][0] * pixelAt(grey, width, height, x - 1, y)) +
        (kernelY[1][1] * pixelAt(grey, width, height, x, y)) +
        (kernelY[1][2] * pixelAt(grey, width, height, x + 1, y)) +
        (kernelY[2][0] * pixelAt(grey, width, height, x - 1, y + 1)) +
        (kernelY[2][1] * pixelAt(grey, width, height, x, y + 1)) +
        (kernelY[2][2] * pixelAt(grey, width, height, x + 1, y + 1))
      );
      var sobelValue = Math.sqrt((pixelX * pixelX) + (pixelY * pixelY));
      sobelData[y*width + x] = sobelValue
    }
  }
  return sobelData;
}

function pixelAt(buffer, width, height, x, y) : number {
  if (x < 0) x = 0
  if (y < 0) y = 0
  if (x > width-1) x = width-1
  if (y > height-1) y = height-1
  //console.log(x + ',' + y + ' = ' + buffer[width*y + x])
  return buffer[width*y + x]
}
