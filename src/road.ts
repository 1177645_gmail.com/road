import fs, { readFile }  from 'fs'
import { fromFile, writeArrayBuffer } from 'geotiff';
import jimp   from 'jimp'

import {Image, ImageBmp, ImageBuffer, ImageMatrix } from './image'
import { Path } from './path'
import * as test from './test'
import createEdgeMapFromImageData from './sobel'
import { Jimp } from '@jimp/core';

let sobelMin = 0.5  // указанную часть (по гистограмме) не ярких точек выкидываем
let crop = false
                let sourceCropStartX = 866
                let sourceCropStartY = 0
                let sourceCropDeltaX = 50
                let sourceCropDeltaY = 360

let cellDeltaX = 50
let cellDeltaY = 20


async function main() {
  process('t1')
  process('t2')
  process('t3')
  process('t4')
}

async function process(src) {
  let image = await jimp.read('/home/omega/TEST/road/' + src + '.bmp')
  let fullWidth = image.getWidth()
  let fullHeight = image.getHeight()
  if (crop) {
    sourceCropDeltaX = sourceCropStartX+sourceCropDeltaX >= fullWidth ? fullWidth - sourceCropStartX : sourceCropDeltaX
    sourceCropDeltaY = sourceCropStartY+sourceCropDeltaY >= fullHeight ? fullHeight - sourceCropStartY : sourceCropDeltaY
    image.crop(sourceCropStartX,sourceCropStartY,sourceCropDeltaX,sourceCropDeltaY)
  }
  image.writeAsync('/home/omega/TEST/road/' + src + '_crop.bmp')
  fullWidth = image.getWidth()
  fullHeight = image.getHeight()

  let sobelArray :Uint8ClampedArray = createEdgeMapFromImageData(image.bitmap)
  cutMin(sobelArray, sobelMin)
  let sobelJimp :jimp = createBmpImageFromRaw(sobelArray, fullWidth, fullHeight)
  sobelJimp.writeAsync('/home/omega/TEST/road/' + src + '_sobel.bmp')

  console.log(fullWidth, fullHeight)
  let fullImage :jimp = new jimp(fullWidth, fullHeight);

  let dx = cellDeltaX
  let dy = cellDeltaY
  for (let x=0; x<fullWidth; x+=dx) {
    for (let y=0; y<fullHeight; y+=dy) {
      console.log('x = ' + x + ' y = ' + y)
      // коррекция значений чтобы не вышли за границы снимка
      let cropDX = x+dx >= fullWidth ? fullWidth - x : dx
      let cropDY = y+dy >= fullHeight ? fullHeight - y : dy
       // отрежем кусок из массива
      let cropAr = cropFromArray(sobelArray, fullWidth, fullHeight,  x, y, cropDX, cropDY) 
//      let cropJimp :jimp = createBmpImageFromRaw(cropAr, cropDX, cropDY)
//      cropJimp.writeAsync('/home/omega/TEST/road/' + src + '_cell_' + x + '_' + y + '.bmp')      

      let cropImage = new ImageBuffer(cropAr, cropDX, cropDY)

      //cropImage.print('Исходная матрица');
      cropImage.makeRoad()
      //cropImage.getRoad().print('Дороги в памяти')
      //cropImage.print('Дороги на снимке')

      saveToImage(fullImage, {x:x, y:y, dx:cropDX, dy:cropDY, image:cropImage})
    }  
  }
  fullImage.writeAsync('/home/omega/TEST/road/' + src + '_road.bmp')

  console.log('OK')
}

function cutMin(buf :Uint8ClampedArray, cut :number) {
  let ret = {}
  if (!buf) return ret
  let rmin = 62000, rmax = 0
  let hist = {}
  for (let i=0; i<buf.length; i++) {
    if (buf[i] != 0 && rmin > buf[i]) rmin = buf[i]
    if (buf[i] != 0 && rmax < buf[i]) rmax = buf[i]
    if (buf[i] != 0) {
      let key = buf[i]
      let v = hist[key]
      if (!v) v = 1 
      else v++
      hist[key] = v
    }
  }
  ret['min'] = rmin
  ret['max'] = rmax

  // вся площадь
  let s = 0
  let revBrightAr = []
  let dirBrightAr = []
  for (let bright in hist) {
    s += Number(bright) * hist[bright]
    dirBrightAr.push(bright)
  }
  let cutS = 0
  let leftCut = 0

  cutS = 0
  for (let bright of dirBrightAr) {
    cutS += bright * hist[bright]
    if (cutS/s > cut) { leftCut = bright; break}
  }

  for (let i=0; i<buf.length; i++) {
    if (buf[i] < leftCut) buf[i] = 0
  }

  return ret
}

function cropFromArray(arr :Uint8ClampedArray, width, height, x, y, dx, dy) :Uint8ClampedArray {
  let ret = new Uint8ClampedArray(dx*dy)
  for (let i=0; i<dx; i++) {
    for (let j=0; j<dy; j++) 
      ret[j*dx+i] = arr[(j+y)*width + (i+x)]
  }
  return ret
}

function saveToImage(fullImage, crop) {
  let { x, y, dx, dy, image } = crop
  
  for (var cropx=0; cropx<dx; cropx++) {
    for (var cropy=0; cropy<dy; cropy++) {
      let v = image.getPixel(cropx,cropy) == 1 ? 255 : 0
      var r, g, b;
      r = g = b = v
      let newX = x+cropx
      let newy = y+cropy
      var num = (b*256*256*256) + (g*256*256) + (r*256) +  255;
      fullImage.setPixelColor(num, newX, newy);
    }
  }
}

function createBmpImageFromRaw(sobel: Uint8ClampedArray, width: number, height: number) :jimp{
  var image = new jimp(width, height);
  for (var x=0; x<width; x++) {
    for (var y=0; y<height; y++) {
      let v = sobel[y*width+x];
      var r = v
      var g = v
      var b = v
      var num = (b*256*256*256) + (g*256*256) + (r*256) +  255;
      image.setPixelColor(num, x, y);
    }
  }
  return image
}

function makeCropRoad(image: ImageBuffer) {
  //image.print('Исходная матрица');
  image.makeRoad()
  //image.getRoad().print('Дороги в памяти')
  //image.print('Дороги на снимке')

  return image
}

async function makeImageWithFlip(imageBase) {

  let imageRow = imageBase.clone()
  imageRow.makeAverage()
  let imageColumn =  new ImageMatrix(imageBase.flip('+'))
  imageColumn.makeAverage()
  imageColumn = new ImageMatrix(imageColumn.flip('-'))
  imageRow.merge(imageColumn)
}


async function mainMatrix() {
  let m = new ImageMatrix(test.t7())
  m.print('Исходная матрица');
  m.makeRoad()
  m.getRoad().print('Дороги в памяти')
  console.log('OK')
}


main()