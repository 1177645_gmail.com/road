import { Bind } from './bind'
import { Group, GroupItem, GroupRow } from './group'
import { Path } from './path'
import jimp   from 'jimp'

export class Image {

  public static useHalfPath = false
  public static usePath180 = true
  public static usePath90 = true
  public static usePath0 = true

  public static minPathByGroup = 7 

  // горизонтальные группы связных точек
  private group : Group = null
  // вертикальные пути - все возможные
  private path : Path = null
  // пути-дороги, которые между границами снимка
  private road : Path = null

  constructor(image: Image) {
  }

  public getWidth() :number                            { throw new Error("abstract method"); }
  public getHeight() :number                           { throw new Error("abstract method"); }
  public getPixel(x:number, y:number) :number          { throw new Error("abstract method"); }
  public setPixel(x:number, y:number, v:number) :void  { throw new Error("abstract method"); }
  public clone(fill:number) :Image                     { throw new Error("abstract method"); }

  public getGroup() : Group { return this.group }
  public getPath()  : Path  { return this.path  }
  public getRoad()  : Path  { return this.road  }

  public setGroup(g:Group) { this.group = g }
  public setPath(p:Path)   { this.path = p  }
  public setRoad(p:Path)   { this.road = p  }

  private fill(val:number) : void {
    for (let x=0; x<this.getWidth(); x++) {
      for (let y=0; y<this.getHeight(); y++)
        this.setPixel(x,y,val)
    }
  }

  public makeAverage() {
  // пытаемся оставить только средние пиксели, которые важны для формирования вертикальных путей

    // формируем группы по имеющимся пикселям
    let group1 = this.makeGroup()
    // уменьшаем кол-во пикселей оставляя в каждой группе только центральный
    this.filterOnAverage(group1)
    // после замены на среднее группы и связи поменялись
    let group2 = this.makeGroup()
    // восстанавливаем разрушенные связи, возвращая часть пикселей
    this.restoreBind(group1, group2)
    // и теперь меняем группы
    this.group = this.makeGroup()
  }

  private makeGroup() : Group {
    // формируем группы для каждой строки - это массив объектов {left: right: bind:}

    let group = new Group(this.getHeight(), this.getWidth())
    // идем по строкам 
    for (let y=0; y<this.getHeight(); y++) {
      // добавляем в массив номер колонки при встрече очередной единицы
      let interval = []
      for (let x=0; x<this.getWidth(); x++) {
        let pix = this.getPixel(x, y);
        // встретили 0
        if (pix == 0) {
          // если перед этим не было единиц - идем дальше
          if (interval.length == 0) continue
          else  {
            // иначе записываем начало и конец интервала и добавляем в массив групп данной строки
            group.getRow(y).push({left:interval[0], right:interval[interval.length-1], bind: [] })
            interval = []
          }
        }
        // очередную единицу просто добавляем в интервал
        else  interval.push(x)
      }
      // если при выходе интервал точек не пустой - то тоже добавляем в группы
      if (interval.length != 0) 
        group.getRow(y).push({left:interval[0], right:interval[interval.length-1], bind: [] })
    }
    // добавить связи к группе
    this.addBind(group)

    return group
  }

  private addBind(group: Group) {
    // формируем связи между группами точек следующей строки на предыдущую и записываем в поле bind

    // фиксируем очередную группу текущей строки
    // вытаскиваем по одной группе из след-й строки
    // если связь есть, то в поле bind (а это массив номеров групп) добавляем группу
    for (let y=0; y<this.getHeight()-1; y++) {
      // по всем группам текущей строки
      for (let i=0; i<group.getRow(y).getLength(); i++) {
        let g = group.getRow(y).getItem(i)
        // левый и правый диапазоны очередной группы текущей строки
        let left = g.left
        let right = g.right
        // они будут связаны с группами которые попадают в увеличенный интервал +-1
        if (left > 0) left--
        if (right < this.getWidth() - 1 ) right++
        // следующая строка
        let nextY = y+1
        // по всем группам следующей строки
        for (let j=0; j<group.getRow(nextY).getLength(); j++) {
          let gNext = group.getRow(nextY).getItem(j)
          let nRight = gNext.right
          let nLeft = gNext.left
          // если границы выходят за диапазон]
          if (! (nLeft > right || nRight < left)  )
            gNext.bind.push(i)
        }
      }
    }
  }

  empty() {  }

  public initPath() : void {
    // формируем все вертикальные пути для заданной строки
    let pathMatrix :number[][] = this.makePath()
    // пути должны содержать хотя бы один не null
    for (let i=pathMatrix.length-1; i>=0; i--)  {
      let ok = false
      for (let j=0; j<this.group.height; j++) {
        if (pathMatrix[i][j] != null) { ok = true; break; }
      }
      if (!ok) 
        pathMatrix.splice(i,1)
    }

    this.path = new Path(pathMatrix)
  }

  public makePath() : number[][] {
    // формируем все вертикальные пути для заданной строки
    let ret = []
    // по всем строкам 
    for (let i=0; i<this.getHeight(); i++) {
      let gRow = this.getGroup().getRow(i)
      // для всех групп в строке
      for (let j=0; j<gRow.getLength(); j++) {
        let g = gRow.getItem(j)
        // связи на предыдущую строку
        let binding = false
        for (let k=0; k<g.bind.length; k++) {
          let prevGroup = g.bind[k]
          // вытаскиваем из массива, все пути, которые заканчиваются группой
          let bindPath = this.extract(ret,i-1,prevGroup)
          if (bindPath.length > 0) {
            // и в каждый добавляем группу k для строки i-1
            this.addGroupInPath(ret, bindPath, i, j)
            binding = true
          }
        } 
        // если нет связей с предыдущей строкой, то просто добавим вершину
        if (!binding) {
          let newPath = new Array(this.getHeight()).fill(null)
          newPath[i] = j
          ret.push(newPath)
        }
      }
    }
    return ret
  }

  private addGroupInPath(full :number[][], bindPath : number[][], index, value) :number[][] {
    let ret = []
    for (let i=0; i<bindPath.length; i++) {
      let path = bindPath[i]
      // если 
      if (path[index] == null) 
        path[index] = value
      else {
        let newPath = new Array(...path)
        newPath[index] = value
        full.push(newPath)
      }
    }
    return ret
  }

  private extract(arr : number[][], last :number, num : number) {
    let ret = []
    for (let i=0; i<arr.length; i++) {
      let path = arr[i]
      if (path[last] == num) ret.push(path)
    }
    return ret
  }

  private makePathI(y :number) :number[][] {
    // Получаем все пути с уровня y-1. Длина каждого пути должна совпасть с y-1. Пишем null если нет совсем групп
    // На уровне y берем все группы 
    // 

    // возвращать будем новый массив, в который добавим увеличившиеся пути или отдельные группы если нет связи
    let ret = []
    //let ret = this.retFromMakePathI

    if (y < 0) throw 'Получен отрицательный уровень'
    // для первой строки связей нет - просто добавляем номера групп
    if (y == 0) {
      for (let k=0; k<this.getGroup().getRow(0).getLength(); k++) {
        let p = []
        p.push(k)
        ret.push(p)
      }
      // если групп нет, то добавим null чтобы не потерялся уровень
      if (ret.length == 0) {
        let p = []
        p.push(null)
        ret.push(p)
      }
      return ret
    }
    // берем все пути с уровня y-1
    let prev = this.makePathI(y-1)
    //let prev = this.retFromMakePathI
    // проверим что все пути длиной y
    for (let i=0; i<prev.length; i++) {
      if (prev[i].length != y) 
        throw 'Ошибочная длина пути'
    }
    // для простоты добавим во все пути null
    for (let i=0; i<prev.length; i++) {
      prev[i].push(null)
    }

    // сюда добавим пути рожденные новыми связями
    let retNew = []
    // начинаем перебирать все группы с уровня y и все связи
    for (let k=0; k<this.getGroup().getRow(y).getLength(); k++) {
      //console.log('Группа ' + k )
      let g = this.getGroup().getRow(y).getItem(k)
      // если связей нет совсем
      if (g.bind.length == 0) {
        // здесь получается разрыв - чтобы не пропали предыдущие уровни добавим null
        let pNew = new Array(y).fill(null)
        pNew.push(k)
        retNew.push(pNew)
      }
      // связи есть, то путь должен быть
      else {
        // пробежимся по путям 
        for (let j=0; j<prev.length; j++) {
          let p = prev[j]
          // в каждом возьмем посл-й номер группы
          let lastPrev = p[p.length-2]  // так как добавили ко всем null реальный стал предпоследним
          for (let i=0; i<g.bind.length; i++) {
            //console.log('\tпоследняя группа пути ' + lastPrev)
            //console.log('\tСвязь ' + g.bind[i])
            // если очередная группа связана с посл-м элементом, то добавляем в существующий путь
            if (lastPrev == g.bind[i]) {
              // если в конце null - значит можно прям в него продолжить путь
              if (p[p.length-1] == null)
                p[p.length-1] = k
              else {
                let pNew = Array.from(p)   
                pNew[p.length-1] = k
                retNew.push(pNew)
              }
            }
          }
        }
      }
    }
    
    prev.push(...retNew)
    return prev
  }

  public makeRoad() : void {
    this.road = new Path()
    // на каждой строке оставляем только средний пиксель с учетом связности между ними
    this.makeAverage()
    //this.getGroup().print('matrix')
    // найдем все пути
    this.initPath()
    //this.getPath().print('Пути')
    // оставим на снимке только пути попадающие на границы (возможно с половинками см. useHalfPath )
    this.makeEdgeRoad()
  }
  
  private makeEdgeRoad() : void {
    // предполагается, что полные дороги пересекают границу снимка в 2-х местах
    // полудороги только в одном
    // пока будем просто выводить и те и другие и попытаемся откинуть не нужные после склейки кусков

    let path = this.path
    let p: number[][] = path.getMatrix()
    let g: Group = this.getGroup()
    
    let width = g.width
    let height = g.height

    // оставим только дороги со след-ми свойствами
      // дорога начинается внизу и кончается вверху
      // дорога начинается слева и кончается справа
      // дорога начинается вверху и кончается слева/справа
      // дорога начинается внизу и кончается слева/справа
      // дорога начинается и заканчивается на верхней или нижней грани

    if (p.length == 0) {
      this.makeFromPath(this.road)
      return 
    }
    // у всех дорог общая длина с учетом null должна быть одинаковая
    for (let i=0; i<p.length; i++) if (p[i].length != height) throw 'Отличается длина дороги'

    let fullPath_180 = [] // пересекают две противоположные границы
    let fullPath_90 = [] // пересекают две соседние границы
    let fullPath_0 = [] // начало и конец только одну границу
    let halfPath = [] // начало ИЛИ конец пересекают только одну границу

    // по всем вертикальным путям
    for (let i=0; i<p.length; i++) {
      let path = p[i]
      // фиксируем { строка: группа: } где начинается и заканчивается дорога
      let start = null
      let finish = null
      // а также внутренние элементы пути, которые касаются боковых сторон
      let side = []
      for (let j=0; j<path.length; j++) {
        // номер группы в пути
        let group = path[j]
        if (group != null) {
          if (start == null) start = { row: j, group: group }
          finish = { row: j, group: path[j] }
        }
        else {
          if (start != null && finish != null) break
        }
        // анализируем все ненулевые элементы пути
        // на протяжении всего  пути касаний может быть много - 
        // все складываем в массив и запоминаем строка, группа, сторона касания
        if (start != null) {
          let item = g.getRow(j).getItem(group)
          // касание левой или правой стороны
          let edge = null
          if (item.left == 0) edge = 0
          if (item.right == width-1) edge = width-1 
          if (edge != null)
            side.push({ row: j, group: group, edge: edge})
        }
      }

      // анализируем вертикальные дороги на предмет касания границ снимка
      if (start == null && finish == null) continue
      if (start == null || finish == null) throw 'Ошибка при анализе вертикальной дороги'

      let up = start.row
      let down = finish.row
      let startLeft = g.getRow(start.row).getItem(start.group).left
      let finishLeft = g.getRow(finish.row).getItem(finish.group).left
      let startRight = g.getRow(start.row).getItem(start.group).right
      let finishRight = g.getRow(finish.row).getItem(finish.group).right
      let full_180 = false  // вход-выход на противоположных сторонах
      let full_90 = false   // вход-выход на соседних сторонах
      let full_0 = false    // вход-выход на одной стороне

      // вертикально
      if (up == 0 && down == height-1) full_180 = true
      // горизонтально
      if (startLeft == 0 && finishRight == width-1) full_180 = true
      if (finishLeft == 0 && startRight == width-1) full_180 = true
      // с поворотами
      else {
        if (startLeft == 0 && finishLeft == 0 && up != down ||                  // вход/выход на левой границе
            startRight == width-1 && finishRight == width-1 && up != down ||    // на правой
            up == 0 && finish == 0 ||                                           // наверху
            up == width-1 && finish == width-1                                  // внизу
            ) full_0 = true

        // слева,справа - вниз
        if (startLeft == 0 || startRight == width-1) {
          if (down == height-1) full_90 = true
        }
        // слева,справа - вверх
        if (finishLeft == 0 || finishRight == width-1) {
          if (up == 0) full_90 = true
        }
        // сверху - влево,вправо
        if (finishLeft == 0 || finishRight == width-1) {
          if (up == 0) full_90 = true
        }
        // снизу - влево,вправо
        if (startLeft == 0 || startRight == width-1) {
          if (down == height-1) full_90 = true
        }
      }
      // начало или конец дороги совпали с границами снимка
      if (full_180) fullPath_180.push(path)
      if (full_90) fullPath_90.push(path)
      if (full_0) fullPath_0.push(path)
      else { 
        // если строго край дороги попадает только на одну границу
        // то можно попробовать оставить для дальнейшего анализа дороги с горизонтальным изгибом
        // в этом случае итоговая дорога получится из 2-х участков
        if (up == 0 || down == height-1 ||                 
            startLeft == 0 || finishLeft == 0 || 
            startRight == width-1 || finishRight== width-1) 
          halfPath.push({ path: path, up: up, down: down, side: side })
      }
      // если часть дороги (не обязательно начало/конец) касается левой или правой стороны снимка
      if (side.length > 0) {
        // то в половинки добавляем 2 новые дороги - путь от точки касания до начала/конца дороги
        for (let ii=0; ii<side.length; ii++) {
          let newSide = []
          let newPath1 = Array.from(path)
          let newPath2 = Array.from(path)
          let rowEdge = side[ii].row
          let groupEdge = side[ii].group

          let item = g.getRow(rowEdge).getItem(groupEdge)
          // касание левой или правой стороны
          let edge = null
          if (item.left == 0) edge = 0
          if (item.right == width-1) edge = width-1 
          if (edge == null) throw 'Нет касания сторон снимка'
          newSide.push({row: rowEdge, group: groupEdge, edge: edge})
          for (let jj=rowEdge+1; jj<=down; jj++) newPath1[jj] = null
          for (let jj=up; jj<rowEdge; jj++) newPath2[jj] = null
          // то формируем новые 2 дороги до и после касания и добавляем в половинки 
          halfPath.push({ path: newPath1, up: up, down: rowEdge, side: newSide })
          halfPath.push({ path: newPath2, up: rowEdge, down: down, side: newSide })
        }
      }
    }

    // делаем копию и выставляем все ячейки в 0
    this.fill(0)
    // дороги заново все формируем
    this.road = new Path([])
    let roadPath = this.road.getMatrix()

    if (Image.useHalfPath) {
      // проверим половинки дорог
      // нет ли среди них пары которая сразу (без посредников) дает полную дорогу
      // соединение через промежуточную дорогу не будем проверять - сильно увеличит время и сложность алгоритма
      // берем первую и сравниваем с оставшимися и т.д.
      for (let i=0; i<halfPath.length; i++) {
        let p1 = halfPath[i].path
        let p1Up = halfPath[i].up
        let p1Down = halfPath[i].down
        let p1Side = halfPath[i].side
        // если половинка касается одной из 4-х сторон
        if (p1Up == 0 || p1Down == height-1 || p1Side.length > 0) {
          for (let k=p1Up; k<=p1Down; k++) {
            //this.addPath(path, k,p1[k])
          }
          roadPath.push(p1)
          continue
        }

        for (let j=i+1; j<halfPath.length; j++) {
          let p2 = halfPath[j].path
          let p2Up = halfPath[j].up
          let p2Down = halfPath[j].down
          let p2Side = halfPath[j].side

          // пересечение по строкам, на которых есть данные - только здесь возможно соединение дорог
          let rowStart = p1Up
          let rowFinish = p1Down
          if (rowStart < p2Up) rowStart = p2Up
          if (rowFinish > p2Down) rowFinish = p2Down
          // если в промежутке между строками есть совпадение групп, то получаем путь из 2-х частей
          for (let k=rowStart; k<=rowFinish; k++) {
            // просто выводим 2 пути никак не обрезая 
            // в этом случае мы можем вывести как завершение какой-то дороги, так и мусор
            // мусор будем отсеивать после соединения элементарных снимков
            if (p1[k] != null && p1[k] == p2[k]) {
              for (let k=p1Up; k<=p1Down; k++) {
                //this.addPath(path, k,p1[k])
              }
              for (let k=p2Up; k<=p2Down; k++) {
                //this.addPath(path, k,p2[k])
              }
              roadPath.push(p1)
              roadPath.push(p2)
              break; 
            }
          }
        }
      }
    }
    // по всем полным путям на противоположные стороны
    if (Image.usePath180) {
      for (let i=0; i<fullPath_180.length; i++) {
        let pI = fullPath_180[i]
        if (this.pathByGroup(pI) < Image.minPathByGroup) continue
        roadPath.push(pI)
      }
    }
    // по всем полным путям на соседние стороны 
    if (Image.usePath90) {    
      for (let i=0; i<fullPath_90.length; i++) {
        let pI = fullPath_90[i]
        if (this.pathByGroup(pI) < Image.minPathByGroup) continue
        roadPath.push(pI)
      }
    }
    // по всем полным путям на одну сторону
    if (Image.usePath0) {    
      for (let i=0; i<fullPath_0.length; i++) {
        let pI = fullPath_0[i]
        if (this.pathByGroup(pI) < Image.minPathByGroup) continue
        roadPath.push(pI)
      }
    }

    this.makeFromPath(this.road)
  }
  
  private pathByGroup(p :number[]) :number {
    let ret = 0
    for (let i=0; i<p.length; i++)
      if (p[i] != null) ret++
    return ret
  }

  // выставляем просто все точки из путей с учетом данных в группе
  // в будущем можно сделать с учетом перехода между точками в пути, чтобы убрать мусор 
  public makeFromPath(path: Path) {
    this.fill(0)
    let m = path.getMatrix()
    for (let i=0; i<m.length; i++) {
      let p = m[i]
      for (let j=0; j<p.length; j++) {
        let row = j
        let numInGroup = p[j]
        if (numInGroup == null) continue
        let g = this.group.getRow(j).getItem(numInGroup)
        if (!g) 
          throw 'Не найдена группа (' + j + ',' + numInGroup + ')'
        for (let i=g.left; i<=g.right; i++) 
          this.setPixel(i,row,1)
      }
    }
  }

  private addPath(path: Path, row: number, group: number) {
    if (group == null) return
    let g: Group = this.getGroup()
    let left = g.getRow(row).getItem(group).left
    let right = g.getRow(row).getItem(group).right

    // ПОКА просто ставим все точки в группе
    for (let i=left; i<=right; i++) 
      this.setPixel(i,row,1)
  }

  filterOnAverage(group: Group) {
    // оставляем среднюю точку из группы - все остальные в 0
    // если группа касается левой или правой границы, то оставляем половину точек до середины от края
    // они важны для соединения тайлов

    for (let y=0; y<this.getHeight(); y++) {
      let groupRow: GroupRow = group.getRow(y)
      for (let i=0; i<groupRow.getLength(); i++) {
          let g: GroupItem = groupRow.getItem(i)
          let xAverage = Math.round((g.left + g.right)/2)
          for (let k=g.left; k<=g.right; k++) {
            if (k == xAverage || 
                g.left == 0 && k < xAverage || 
                g.right == this.getWidth()-1 && k > xAverage)
              continue  
            else 
              this.setPixel(k,y,0)
          }
      }
    }
  }

  private minusTwoArray(b1 :number[], b2 :number[]) {
    let ret = []
    // b1 больше чем b2
    for (let i=0; i<b1.length; i++) {
      if (b2.includes(b1[i])) continue
      ret.push(b1[i])
    }
    return ret
  }

  private restoreBind(group1: Group, group2: Group) {
    // при замене группы точек на среднюю возможна потеря связности между группами
    // кол-во групп в каждой строке не меняется и следов-о связность между ними должна сохраниться
    // пусть А - группа из предыдущеей строки   В - из след-й строки
    // при обнаружении того, что пропала связь между А и В - вычисляем кол-во точек, которые нужно добавить
    // сначала добавляем все возможные точки из А и если не хватает из В
    // вход     усреднение  восстановлено   
    // --111    ---1-       --11- 
    // 11---    1----       11---

    // group1 - было в исходных данных
    // group2 - стало после усреднения
    // идем по строкам начиная со 2-й (в первой нет связей)
    for (let y=1; y<this.getHeight(); y++) {
      let gRow1: GroupRow = group1.getRow(y)
      let gRow2: GroupRow = group2.getRow(y)
      // число групп в каждой строке не должно поменяться
      if (gRow1.getLength() != gRow2.getLength()) throw 'Число групп в строке не равны'
      // проверяем связи
      for (let k=0; k<gRow1.getLength(); k++) {
        let b1: number[] = gRow1.getItem(k).bind
        let b2: number[] = gRow2.getItem(k).bind
        if (b1.length < b2.length) 
          throw 'Связность не может увеличиться'
        if (b1.length == b2.length) continue
        // определяем какие интервалы пропали
        let lost = this.minusTwoArray(b1,b2)
        // текущий интервал gRow2[k]
        let gRowCurrent = gRow2[k]
        // старый был такой
        let gRowFullCurrent = group1.getRow(y).getItem(k)
        // в текущем потеряны связи на интервалы с предыдущего уровня
        for (let i=0; i<lost.length; i++) {
          let lostInterval = lost[i]
          // был такой интервал на пред-й строке
          let gPrevFull = group1.getRow(y-1).getItem(lostInterval)
          // стал таким
          let gPrevBrief = group2.getRow(y-1).getItem(lostInterval)

          // текущий интервал
          let gCurrentBrief = gRow2.getItem(k)
          // был таким
          let gCurrentFull = gRow1.getItem(k)
//          console.log(gPrevFull.left + '-' + gPrevFull.right + '->' + gPrevBrief.left + '-' + gPrevBrief.right)
//          console.log(gCurrentFull.left + '-' + gCurrentFull.right + '->' + gCurrentBrief.left + '-' + gCurrentBrief.right)
//          console.log('--------')

          // если предыдущая строка лежит справа
          if (gCurrentBrief.right < gPrevBrief.left) {
            let diff = gPrevBrief.left - gCurrentBrief.right -1
            // и теперь эту разницу заполняем c предыдущего уровня
            let count = 0
            for (let xx=gPrevBrief.left-1; xx >= gPrevFull.left && count < diff; xx-- ) {
              this.setPixel(xx,y-1,1)
              count++;
            }
            // и если не хватает, то с текущего
            if (count < diff ) {
              for (let xx=gCurrentBrief.right+1; xx <= gCurrentFull.right && count <= diff ; xx++ ) {
                this.setPixel(xx,y,1)
                count++;
              }
            }
          }
          // иначе если слева
          else {
            let diff = gCurrentBrief.left - gPrevBrief.right -1
            // и теперь эту разницу заполняем c предыдущего уровня
            let count = 0
            for (let xx=gPrevBrief.right+1; xx <= gPrevFull.right && count < diff; xx++) {
              this.setPixel(xx,y-1,1)
              count++;
            }
            // и если не хватает, то с текущего
            if (count < diff ) {
              for (let xx=gCurrentBrief.left-1; xx >= gCurrentFull.left && count <= diff ; xx-- ) {
                this.setPixel(xx,y,1)
                count++;
              }
            }
          }
        }
      }
    }
  }

  public flip(to :string) {
    // поворачиваем матрицу на 90град

    let flip90 = []
    // по часовой
    if (to == '+') {
      // фиксируем колонку с первой
      for (let x=0; x<this.getWidth(); x++) {
        flip90[x] = []
        // и для нее начинаем добавлять с последней строки значения
        for (let y=this.getHeight()-1; y>=0; y--) {
          flip90[x].push(this.getPixel(x,y))
        }
      }
    }
    // иначе против часовой
    else {
      // фиксируем колонку с последней
      for (let x=this.getWidth()-1; x>=0; x--) {
        let row = this.getWidth()-1 - x
        flip90[row] = []
        // и для нее начинаем добавлять с первой строки значения
        for (let y=0; y<this.getHeight(); y++) {
          flip90[row].push(this.getPixel(x,y))
        }
      }
    }
    return flip90
  }
  
  public merge(image2 :Image) {
    // объединяем матрицы

    for (let y=0; y<this.getHeight(); y++) {
      for (let x=0; x<this.getWidth(); x++) {
        let v1 = this.getPixel(x,y,)
        let v2 = image2.getPixel(x,y,)
        let v = v1 | v2
        this.setPixel(x,y,v)
      }
    }
  }

  public print(rem :string ) :void {
    if (rem) console.log(rem)
    for (let y=0; y<this.getHeight(); y++) {
      let row = ''
      for (let x=0; x<this.getWidth(); x++) {
        let v = this.getPixel(x,y) == 1 ? '*' : '-' 
        row += v + ' ' 
      }
      console.log(row)
    }
    console.log(' ')
  }
}

export class ImageBmp extends Image {

  bmp = null

  constructor(image) {
    super(image)    
    this.bmp = image
  }
  getImage() { return this.bmp }

  getWidth() { return this.bmp.getWidth(); }
  getHeight() { return this.bmp.getHeight(); }

  getPixel(x,y) { 
    let v = jimp.intToRGBA(this.bmp.getPixelColor(x, y))
    return v.r
   }

   setPixel(x, y, value) {
    let v = jimp.rgbaToInt(value, value, value, 0)
    this.bmp.setPixelColor(v, x, y)
   }
}

export class ImageBuffer extends Image {

  buffer = null
  width = null
  height = null

  constructor(buffer, w, h) {
    super(buffer)    
    this.buffer = buffer
    this.width = w
    this.height = h
  }
  getImage() { return this.buffer }

  getWidth() { return this.width; }
  getHeight() { return this.height; }

  getPixel(x,y) { 
    let value = this.buffer[y*this.getWidth() + x]
    if (value > 0) return 1
    else return 0
   }

   setPixel(x, y, value) {
    let v = value > 0 ? 1 : 0
    this.buffer[y*this.getWidth() + x] = v
   }

   clone(fill:number) :Image {
    let length = this.width * this.height
    let newBuffer = new Uint8ClampedArray(length);
    for (let i=0; i<length; i++) {
      if (fill == undefined) newBuffer[i] = this.buffer[i]
      else newBuffer[i] = fill
    } 
    let newImage = new ImageBuffer(newBuffer, this.width, this.height)
    newImage.setGroup(this.getGroup())
    newImage.setPath(this.getPath())
    newImage.setRoad(this.getRoad())
    return newImage
  }

}

export class ImageMatrix extends Image {

  matrix = null

  constructor(matrix) {
    super(matrix)
    // проверим, что все строки одной длины
    if (!matrix || matrix.length == 0) return 
    let rowLength = matrix[0].length
    for (let i=0; i<matrix.length; i++) {
      if (matrix[i].length != rowLength) throw 'Строки матрицы разной длины'
    }
    this.matrix = matrix
  }


  getImage() { return this.matrix }

  getWidth() { return this.matrix[0].length; }
  getHeight() { return this.matrix.length; }

  getPixel(x,y) { 
    let cell = this.matrix[y][x] 
    return cell
  }
 
  setPixel(x, y, value) {
      let v = value
      this.matrix[y][x] = v
  }
 
  clone(fill) {
    let newM = new Array(this.getHeight())
    for (let i=0; i<newM.length; i++) newM[i] = new Array (this.getWidth())
    let newImage = new ImageMatrix(newM)
    for (let y=0; y<this.getHeight(); y++) {
      for (let x=0; x<this.getWidth(); x++) {
        let v = this.getPixel(x,y)
        if (fill == undefined) newImage.setPixel(x,y,v)
        else newImage.setPixel(x,y,fill)
      }
    }
    return newImage
  }
}