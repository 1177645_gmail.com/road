  // связи между строками
export class Bind {

  bind = null
  // 0 - нет связи
  // массив 1 или 2 элемента - номера интервала с которым связан элемент из предыдущей строки
  getBind(x,y) { return this.bind[y][x] }
  setBind(x,y,value) {
    let v = this.bind[y][x]
    if (Array.isArray(v)) v.push(...value)
    else this.bind[y][x] = value 
  }

  constructor(width :number, height :number) {
    this.bind = new Array(height)
    for (let i=0; i<this.bind.length; i++)
      this.bind[i] = new Array(width)
  }
} 


